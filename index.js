// 1. Document Object Model - інтерактивний інтерфейс, який дозволяє взаємодію JS з HTML.
// 2. innerHTML - показує структуру HTML, а innerText вміст
// 3. querySelector або querySelectorAll кращі варіанти, тако можна звернутися по айді, тег нейму та класс нейму.






// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let p = document.querySelectorAll('p');
p.forEach((elem => elem.style.backgroundColor = '#ff0000'))
console.log(p)

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let list = document.querySelector('#optionsList')
console.log(list)
console.log(list.parentElement)
console.log(list.childNodes)


// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
// Елемент відсутній, тому був створений і потім доданий

let testP = document.querySelector('#testParagraph')
testP.innerText = 'This is a paragraph'

//
// // 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
//
let elems = document.querySelectorAll('.main-header li')
console.log(elems)
elems.forEach(elem => elem.classList.add('nav-item'))
//
// // 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
//
let del = document.querySelectorAll('.section-title')
console.log(del)
del.forEach(elem => elem.classList.remove('section-title'))
console.log(del)


